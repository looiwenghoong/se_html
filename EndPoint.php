<?php
$servername = "localhost";
$username = "admin";
$password = "admin123";
$dbname = "assignmentdb";

// Create connection
$conn = new mysqli($servername, $username, $password,$dbname);

// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} 
//print_r("Connected successfully");


//GET REQUEST
if(!empty($_GET["query"])){
  $data = json_decode($_GET["query"],true);
  //echo $_GET["query"];
  if(!(isset($data["table"])) || $data["table"]==""){
  	echo "Include the Table of DATABASE";
  }else{
  	if(strtolower($data["table"]) == "clientuser"){
  		if(!(isset($data["Username"]))|| $data["Username"]=="" || !(isset($data["Password"]))|| $data["Password"] == ""){
	  		echo "No Username or Password Entered";
	  	}else{
			$QuerySQL = "SELECT Id,FirstName,LastName,Image,Username,Email FROM " . $data["table"] ." WHERE Username = \"".$data["Username"]."\" AND Password = \"". $data["Password"]."\";";  	  		
	  		
	  		$result = $conn->query($QuerySQL);

	  		if (mysqli_num_rows($result) > 0) {
	  			$data = array();
			  	while($r = mysqli_fetch_assoc($result)) {
				    $data[] = $r;
				}
				if((isset($data["Password"]))){
						unset($data["Password"]);
				}

			  	echo '{ "status":"successful","data" : '.json_encode($data[0]).'}';
	  		}else{
  				echo '{ "status":"failed","error" :"Invalid Username or Password" }';
  			}
	  	}
  	}else{
	  	if(empty($data["filter"])){
	  		$queryFilter = "";
	  	}else{
	  		$queryFilter = " WHERE " . $data["filter"];
	  	}
	  	if($data["table"]=="custom"){
	  		$QuerySQL = $data["custom"];
	  	}else{
	  		$QuerySQL = "SELECT * FROM " . $data["table"] . $queryFilter;  
	  	}
	  	
	  	//echo $QuerySQL;
	  	$result = $conn->query($QuerySQL);
		if ($result->num_rows > 0) {
		  	$data = array();
		  	while($r = mysqli_fetch_assoc($result)) {
			    $data[] = $r;
			}
			//print_r($data);
		  	echo '{ "status":"successful","data" : '.json_encode($data).'}';
  		}else{
  			echo '{ "status":"failed","error" :"No Data Found in the Package ID" }';
  			
  		}
  	}
  }
}else{

//POST request
	if(empty($_POST["table"])||in_array(strtolower($_POST["table"]),["package","hotel","food","room"])){
		echo "Please include table field in form";
	}elseif(!empty($_POST["Id"])){
		
		$data = $_POST;
		unset($data['table']);
		unset($data['Id']);
		$QuerySQL = "UPDATE ".$_POST["table"] . " SET ";
		if(!empty($_FILES["Image"])){
			$target_dir = "Media/".$data["table"]."/";
			$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
			if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"&& $imageFileType != "gif" ) {
				    echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
				    
			}else{
				$target_file = $target_dir . basename($data['table']. $data['Id'].".".$imageFileType);

				if (move_uploaded_file($_FILES["test"]["tmp_name"], $target_file)) {
				        echo "The file ". basename( $_FILES["test"]["name"]). " has been uploaded.";
				    	$data["Image"] = $target_file;
				    } else {
				        echo "Sorry, there was an error uploading your file.";
				    }
			}
		}
		foreach($data as $key => $val) {
			if(is_string($val)){
	        	$val = "\"".$val."\"";
	        }
	        $QuerySQL = $QuerySQL . $key . " = " . $val . " , ";
	    }
	    $QuerySQL = substr($QuerySQL, 0, -2) . "WHERE " . $_POST["table"] .".`Id` = " . $_POST["Id"];
		if ($conn->query($QuerySQL) === TRUE) {
			if(strtolower($_POST["table"]) == "clientuser"){
					$GetSQL = 'SELECT Id,FirstName,LastName,Image,Username,Email FROM ' . $_POST["table"] . ' WHERE Id = ' .$_POST["Id"] .';';	
			}else{
					$GetSQL = 'SELECT * FROM ' . $_POST["table"] . ' WHERE Id = ' .$_POST["Id"] .';';	
			}
			
		    $result = $conn->query($GetSQL);
				if ($result->num_rows > 0) {
				  	$data = array();
				  	while($r = mysqli_fetch_assoc($result)) {
					    $data[] = $r;
					}
				  	echo '{ "status":"successful","data" : '.json_encode($data).'}';
		  		}else{
		  			echo "no result found";
		  		}


		}else {
		    echo '{ "status": "failed" , "error" : "' . $conn->error .'" }';
		}

	}else{

		$data = $_POST;
		unset($data['table']);
		unset($data['bookingId']);
		$QuerySQL = "INSERT INTO ".$_POST["table"]." (";
		//echo $QuerySQL;
		$temp = "(";
		foreach($data as $key => $val) {
	        $QuerySQL = $QuerySQL . $key . ",";
	        if (empty($val)){
	        	echo "Data no fill cant insert to DATABASE";
	        	return;
	        }
	        if(is_string($val)){
	        	$val = "\"".$val."\"";
	        }
	    	$temp =  $temp .$val.",";
	    }
			$QuerySQL = substr($QuerySQL, 0, -1) . ") VALUES " . substr($temp, 0, -1).")";
			//echo $QuerySQL;
		if($conn->query($QuerySQL) === TRUE) {
			$last_id = $conn->insert_id;
		    
		    if(strtolower($_POST["table"]) == "payment"){
		    	$QuerySQL = "UPDATE booking SET Payment = ".$last_id." WHERE Id = ".$_POST['bookingId'];
		    	//echo $QuerySQL;
		    	if($conn->query($QuerySQL) === TRUE) {
		    		echo '{ "status": "successful" , "data" : "' .'Table ' . $_POST["table"] .'  successfully Insert a new data" }';
		    	}else{
		    		echo '{ "status": "failed" , "error" : "' . $conn->error .'" }';		
		    	}

		    }else{
		    	echo '{ "status": "successful" , "data" : "' .'Table ' . $_POST["table"] .'  successfully Insert a new data" }';	
		    }
		}else {
		    echo '{ "status": "failed" , "error" : "' . $conn->error .'" }';
		}

	};

//UPDATE sql		
//UPDATE  

}


?> 