jQuery(document).ready(function($){
	//toggle 3d navigation
	$('.cd-3d-nav-trigger').on('click', function(){
		toggle3dBlock(!$('.cd-header').hasClass('nav-is-visible'));
	});

	//select a new item from the 3d navigation
	$('.cd-3d-nav').on('click', 'a', function(){
		var selected = $(this);
		selected.parent('li').addClass('cd-selected').siblings('li').removeClass('cd-selected');
	});

	function toggle3dBlock(addOrRemove) {
		if(typeof(addOrRemove)==='undefined') addOrRemove = true;	
		$('.cd-header').toggleClass('nav-is-visible', addOrRemove);
		$('.cd-3d-nav-container').toggleClass('nav-is-visible', addOrRemove);
		$('main').toggleClass('nav-is-visible', addOrRemove).one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			//fix marker position when opening the menu (after a window resize)
			addOrRemove && updateSelectedNav();
		});
	}

	$.fn.removeClassPrefix = function(prefix) {
	    this.each(function(i, el) {
	        var classes = el.className.split(" ").filter(function(c) {
	            return c.lastIndexOf(prefix, 0) !== 0;
	        });
	        el.className = $.trim(classes.join(" "));
	    });
	    return this;
	};
});

function toggle()
{ 
	var overlay = document.getElementById("login_main_overlay");
	var login = document.getElementById("login_section_title");
	var signUp = document.getElementById("signup");


	signUp.style.display = "none";

	if(document.getElementById("navbar_account").innerHTML.match("Login"))
	{
		if(overlay.style.display == "" || overlay.style.display == "none" )
		{
			overlay.style.display = "block";
			login.style.display = "block";
			overlay.style.webkitTransitionDelay = 0.5;
		}
	}
	else
	{
		document.getElementById("navbar_account").addEventListener("click", window.open("account.html", "_self"));	
	}
}



function off()
{
	var overlay_off = document.getElementById("login_main_overlay");
	overlay_off.style.display = "none";

	document.getElementById("login_form").reset();
	document.getElementById("signUp_form").reset();
}

function destroyDiv()
{
	var signUp = document.getElementById("signup");
	var login = document.getElementById("login_section_title");
	
	signUp.style.display = "block";
	login.style.display = "none";
}

// Create account function, post data to DB
// return to the sign up div
function createAccount(oFormElement)
{	
	QueryPost(oFormElement);	
	var signUp = document.getElementById("signup");
	var login = document.getElementById("login_section_title");
	
	signUp.style.display = "none";
	login.style.display = "block";
}

function QueryPost(oFormElement){
   
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.responseText);
            }
        };
    
    xmlhttp.open("post", oFormElement.action);
    var formData = new FormData(oFormElement);
    formData.append("table","clientuser");
    formData.delete('retype');
    xmlhttp.send(formData);

}

// Get email input
var Email = document.getElementById("Email");

// Listen for keystroke events
Email.onchange = function (e) 
{
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if (document.getElementById("Email").value.match(mailformat))
	{
		alert("true");
	}
	else
	{
		alert("You have entered an invalid email address!");
	}        
};

var retype = document.getElementById("retype");
retype.onchange = function (e)
{
	var pass = document.getElementById("password").value;

	if(retype.value.match(pass))
	{
		alert("true");
	}
	else
	{
		alert("False");
	}
}



var dummy = '{"status": "success", "data": {"id": 1, "FirstName" : "looi", "LastName" : "Weng Hoong", "Username" : "Looi", "Email": "looiwenghoong@gmail.com"}}';

// check if the cookie exist
function checkCookie()
{   
	var account = getCookie("accountCookie");

	// console.log(JSON.parse(account));
	if(account != "" || account != null)
	{
		var user=JSON.parse(account);
		document.getElementById("navbar_account").innerHTML = user.data.Username;
	}
}

function setCookie(cname, cvalue, cexdays)
{
    var d = new Date();
    d.setTime(d.getTime() + (cexdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);

    console.log(decodedCookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}


function login(username , password)
{	
	var str = '"Username" : "'+username+'", "Password": "'+ password +'"';
	LoginQuery(str);

	
}

function LoginQuery(str) {
	var query='{ "table": "clientuser" ,'+ str +'}' 
	console.log(query);
	method = "get";
    if (method.length == 0) { 
        console.log("wrong method")
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
         		
            		console.log(this.responseText);

                var obj = JSON.parse(this.responseText);

				if(obj.status.match("success"))
				{
					location.reload();

					setCookie("accountCookie",this.responseText, 30);

					var user=JSON.parse(getCookie("accountCookie"));

					// login success
					// change the navbar account name
					document.getElementById("navbar_account").innerHTML = user.data.Username;
					
					// close the login section 
					var overlay_off = document.getElementById("login_main_overlay");
					overlay_off.style.display = "none";
				}else{
					alert(obj.error);
				}
            }
        };
        xmlhttp.open("GET", "EndPoint.php?query="+query, true);
        xmlhttp.send();
    }
}