function GetUrlValue(VarSearch){
    var SearchString = window.location.search.substring(1);
    var VariableArray = SearchString.split('&');
    for(var i = 0; i < VariableArray.length; i++){
        var KeyValuePair = VariableArray[i].split('=');
        if(KeyValuePair[0] == VarSearch){
            return KeyValuePair[1];
        }
    }
}

var user_id;

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);

    console.log(decodedCookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

function checkCookie()
{   
	var account = getCookie("accountCookie");
	// console.log(JSON.parse(account));
	if(account != "" || account != null)
	{
		var user=JSON.parse(account);

        user_id = user.data.Id;
        document.getElementById("navbar_account").innerHTML = user.data.Username;
	}
}

var querydata = new Object();

Query("hotel","PackageId ="+GetUrlValue("packageId"));

function makeGenerator(){
    var myObj = querydata['hotel'];

    for(var i = 0; i < myObj.length; i++)
    {
        var j = myObj[i];

        var hotel_container = document.createElement("div");
        hotel_container.setAttribute("id","hotel_container"+j.Id);
        hotel_container.setAttribute("class","hotel_container");
        hotel_container.setAttribute("hotel_id",j.Id);
        hotel_container.setAttribute("onclick", "chooseHotel(this.id)");

        // Images container
        var image = document.createElement("img");
        image.setAttribute("id", "image_container");
        image.classList.add("image_container");

        var hotel_image_container = document.createElement("div");
        hotel_image_container.setAttribute("id", "hotel_image_container");
        hotel_image_container.classList.add("hotel_image_container");

        var hotel_image = document.createElement("img");
        image.setAttribute("id", "custom_image");
        image.setAttribute("alt", "hotel_image");
        image.style.height = "100%";
        image.style.width = "100%";
        image.style.maxHeight = "200px";
        image.src = j.Image; 



        var hotel_details = document.createElement("div");
        hotel_details.setAttribute("id","hotel_details");
        hotel_details.classList.add("hotel_details");

        var hotel_name_container = document.createElement("div");
        hotel_name_container.setAttribute("id", "hotel_name_container");
        hotel_name_container.classList.add("hotel_name_container");
        var hotel_name = document.createElement("p");
        hotel_name.innerHTML = "Hotel: " + j.HotelName;
        hotel_name_container.appendChild(hotel_name);

        var hotel_des_container = document.createElement("div");
        hotel_des_container.setAttribute("id", "hotel_des_container");
        hotel_des_container.classList.add("hotel_des_container");
        var hotel_des = document.createElement("p");
        hotel_des.innerHTML = "Description: " + j.Description;
        hotel_des_container.appendChild(hotel_des);

        var hotel_rate_container = document.createElement("div");
        hotel_rate_container.setAttribute("id", "hotel_rate_container");
        hotel_rate_container.classList.add("hotel_rate_container");
        var hotel_rate = document.createElement("p");
        hotel_rate.innerHTML = "Rating: " + j.Rate + " stars";
        hotel_rate_container.appendChild(hotel_rate);

        var selection = document.createElement("input");
        selection.setAttribute("id", "hotel_input"+j.Id);
        selection.setAttribute("type", "radio");
        selection.setAttribute("hidden", true);
        selection.setAttribute("name", "HotelId");
        selection.setAttribute("value", j.Id);


        hotel_details.appendChild(hotel_name_container);
        hotel_details.appendChild(hotel_des_container);
        hotel_details.appendChild(hotel_rate_container);
        hotel_details.appendChild(selection);
        

        hotel_image_container.appendChild(image);
        hotel_container.appendChild(hotel_image_container);
        hotel_container.appendChild(hotel_details);

        document.getElementById("hotel_form").appendChild(hotel_container);

    }


    var lunchObj = querydata['food'];

    for(var l = 0; l < lunchObj.length; l++)
    {
        var k = lunchObj[l];

        var lunch_container = document.createElement("div");
        lunch_container.setAttribute("id","lunch_container"+k.Id);
        lunch_container.setAttribute("class","lunch_container");
        lunch_container.setAttribute("lunch_id",k.Id);
        lunch_container.setAttribute("onclick", "chooseLunch(this.id)");

        var lunch_image_container = document.createElement("div");
        lunch_image_container.setAttribute("id", "lunch_image_container");
        lunch_image_container.classList.add("lunch_image_container");

        var lunch_image = document.createElement("img");
        lunch_image.setAttribute("id", "lunch_image");
        lunch_image.setAttribute("alt", "lunch_image");
        lunch_image.style.height = "100%";
        lunch_image.style.width = "100%";
        lunch_image.style.maxHeight = "200px";
        lunch_image.src = k.Image; 

        var lunch_details = document.createElement("div");
        lunch_details.setAttribute("id","lunch_details");
        lunch_details.classList.add("lunch_details");

        var lunch_name_container = document.createElement("div");
        lunch_name_container.setAttribute("id", "lunch_name_container");
        lunch_name_container.classList.add("lunch_name_container");
        var lunch_name = document.createElement("p");
        lunch_name.innerHTML = k.Title;
        lunch_name_container.appendChild(lunch_name);

        var lunch_des_container = document.createElement("div");
        lunch_des_container.setAttribute("id", "lunch_des_container");
        lunch_des_container.classList.add("lunch_des_container");
        var lunch_des = document.createElement("p");
        lunch_des.innerHTML = "Description: " + k.Description;
        lunch_des_container.appendChild(lunch_des);

        var lunch_selection = document.createElement("input");
        lunch_selection.setAttribute("id", "lunch_input"+k.Id);
        lunch_selection.setAttribute("type", "radio");
        lunch_selection.setAttribute("name", "LunchId");
        lunch_selection.setAttribute("value", k.Id);
        lunch_selection.setAttribute("hidden", true);


        lunch_image_container.appendChild(lunch_image);
        lunch_details.appendChild(lunch_name_container);
        lunch_details.appendChild(lunch_des_container);
        lunch_details.appendChild(lunch_selection);
        


        lunch_container.appendChild(lunch_image_container);
        lunch_container.appendChild(lunch_details);

        document.getElementById("lunch_form").appendChild(lunch_container);

    }


    var dinnerObj = querydata['food'];

    for(var d = 0; d < dinnerObj.length; d++)
    {
        var din = dinnerObj[d];

        var dinner_container = document.createElement("div");
        dinner_container.setAttribute("id","dinner_container"+din.Id);
        dinner_container.setAttribute("class","dinner_container");
        dinner_container.setAttribute("dinner_id",din.Id);
        dinner_container.setAttribute("onclick", "chooseDinner(this.id)");

        var dinner_image_container = document.createElement("div");
        dinner_image_container.setAttribute("id", "dinner_image_container");
        dinner_image_container.classList.add("dinner_image_container");
        
        var dinner_image = document.createElement("img");
        dinner_image.setAttribute("id", "dinner_image");
        dinner_image.style.height = "100%";
        dinner_image.style.width = "100%";
        dinner_image.style.maxHeight = "200px";
        dinner_image.src = din.Image; 


        var dinner_details = document.createElement("div");
        dinner_details.setAttribute("id","dinner_details");
        dinner_details.classList.add("dinner_details");

        var dinner_name_container = document.createElement("div");
        dinner_name_container.setAttribute("id", "dinner_name_container");
        dinner_name_container.classList.add("dinner_name_container");
        var dinner_name = document.createElement("p");
        dinner_name.innerHTML = din.Title;
        dinner_name_container.appendChild(dinner_name);

        var dinner_des_container = document.createElement("div");
        dinner_des_container.setAttribute("id", "dinner_des_container");
        dinner_des_container.classList.add("dinner_des_container");
        var dinner_des = document.createElement("p");
        dinner_des.innerHTML = "Description: " + din.Description;
        dinner_des_container.appendChild(dinner_des);

        var dinner_selection = document.createElement("input");
        dinner_selection.setAttribute("id", "dinner_input"+din.Id);
        dinner_selection.setAttribute("type", "radio");
        dinner_selection.setAttribute("name", "DinnerId");
        dinner_selection.setAttribute("value", din.Id);
        dinner_selection.setAttribute("hidden", true);


        dinner_details.appendChild(dinner_name_container);
        dinner_details.appendChild(dinner_des_container);
        dinner_details.appendChild(dinner_selection);

        dinner_image_container.appendChild(dinner_image);
        dinner_container.appendChild(dinner_image_container);
        dinner_container.appendChild(dinner_details);

        document.getElementById("dinner_form").appendChild(dinner_container);

    }

    document.querySelector("#date_input").valueAsDate = new Date();
}
// choose hotel and display the food for the ID
function chooseHotel(hotel_id)
{
    var Hid = document.getElementById(hotel_id).getAttribute("hotel_id");
    document.getElementById("hotel_input"+Hid).checked = true;
    
    var c = document.getElementsByClassName("hotel_container");
    for(var child = 0; child < c.length; child++)
    {
        c[child].style.backgroundColor = "#fce6cc";
    }

    document.getElementById("hotel_container"+Hid).style.background = "#ffd8a8";
}

function chooseLunch(lunch_id)
{
    var Lid = document.getElementById(lunch_id).getAttribute("lunch_id");
    document.getElementById("lunch_input"+Lid).checked = true;

    var c = document.getElementsByClassName("lunch_container");
    for(var child = 0; child < c.length; child++)
    {
        c[child].style.backgroundColor = "#fce6cc";
    }

    document.getElementById("lunch_container"+Lid).style.background = "#ffd8a8";
}

function chooseDinner(dinner_id)
{
    var Did = document.getElementById(dinner_id).getAttribute("dinner_id");
    document.getElementById("dinner_input"+Did).checked = true;

    var c = document.getElementsByClassName("dinner_container");
    for(var child = 0; child < c.length; child++)
    {
        c[child].style.backgroundColor = "#fce6cc";
    }

    document.getElementById("dinner_container"+Did).style.background = "#ffd8a8";
}

// function checkDate()
// {
//     var selectedDay = document.getElementById("date_input").value;
//     var today = new Date();
// var dd = today.getDate();
// // var mm = today.getMonth()+1; //January is 0!
// // var yyyy = today.getFullYear();

// // if(dd<10) {
// //     dd = '0'+ dd
// // } 

// // if(mm<10) {
// //     mm = '0'+ mm
// // } 

// // today = mm + '/' + dd + '/' + yyyy;
//     console.log(today);
// }


// function to checkout the selected items and pass to database
function booking(custom_form)
{
    var package_id = GetUrlValue('packageId');
    
    console.log(user_id);
    var formData = new FormData(custom_form);
    formData.append("PackageId",package_id);
    formData.append("ClientUser",user_id);
    formData.append("table","booking");
    for (var key of formData.keys()) {
        console.log(key); 
    }
    console.log(formData);
    QueryPost(formData);
}

function QueryPost(formData){
   
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.responseText);

                var obj = JSON.parse(this.responseText);

                if(obj.status.match("successful"))
                {   

                    window.location = "myBooking.html";

                }else{
                    alert(obj.error);
                }
            
            }
        };
    
    xmlhttp.open("post", 'EndPoint.php');
    
    xmlhttp.send(formData);

}


function Query(table,filter){
    var query='{ "table": "'+ table +'","filter":"'+filter+'"}'; 
    console.log(query);
    method = "get";
    if (method.length == 0) { 
        console.log("wrong method")
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                
                    console.log(this.responseText);

                var obj = JSON.parse(this.responseText);

                if(obj.status.match("success"))
                {   
                    querydata[table] = obj["data"];
                    if(querydata['food']){
                        console.log(querydata);
                        makeGenerator();
                    }else{
                        Query("food","PackageId ="+GetUrlValue("packageId"));
                    }

                }else{
                    alert(obj.error);
                }
            }
        };
        xmlhttp.open("GET", "EndPoint.php?query="+query, true);
        xmlhttp.send();
    }
}
