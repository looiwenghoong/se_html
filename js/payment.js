if((document.getElementById('paymentContainer').offsetHeight + document.getElementById('paymentTitle').offsetHeight) >= parseInt(document.getElementById('paymentBody').offsetHeight)){
	document.getElementById('paymentBody').classList.add("paymentBodyOverflow");
	document.getElementById('paymentBody').classList.remove("paymentBody");
}

Query(GetUrlValue("bookingId"))
var totalPrice = 0;
function paymentSummaryGenerator(data){

    console.log(document.getElementById("paymentBookingId"));
    document.getElementById("paymentBookingId").innerHTML = data.Id;
    document.getElementById("summaryImage").src = data.Image;
    document.getElementById("summaryPackageTitle").innerHTML = data.Title;
    document.getElementById("summaryPackagePrice").innerHTML = data.Price;
    document.getElementById("summaryOderNOP").innerHTML = data.NoOfPax;
    document.getElementById("summaryCount").innerHTML = data.NoOfPax +" X " + data.Price ;
    document.getElementById("summaryTotal").innerHTML = "RM" + (data.NoOfPax * data.Price) ;
    totalPrice = data.NoOfPax * data.Price;
    document.getElementById("paymentSubmit").onclick = function(){
        var formData = new FormData(this.parentElement);
        formData.append("table","payment");
        formData.append("bookingId",GetUrlValue("bookingId"));
        formData.append("Price",totalPrice);
        QueryPost(formData);
    }
} 

function GetUrlValue(VarSearch){
    var SearchString = window.location.search.substring(1);
    var VariableArray = SearchString.split('&');
    for(var i = 0; i < VariableArray.length; i++){
        var KeyValuePair = VariableArray[i].split('=');
        if(KeyValuePair[0] == VarSearch){
            return KeyValuePair[1];
        }
    }
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function QueryPost(formData){
   
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.responseText);

                var obj = JSON.parse(this.responseText);

                if(obj.status.match("success"))
                {   
                    console.log("bla")
                    window.location = "myBooking.html";
                                        

                }else{
                    alert(obj.error);
                }
            
            }
        };
    
    xmlhttp.open("post", 'EndPoint.php');
    
    xmlhttp.send(formData);

}


	
function Query(BookingId){
    var query='{ "table": "custom","custom":"SELECT a.*, b.Title , b.Price,b.Image from booking as a inner join package as b on a.PackageId = b.Id WHERE a.Id = '+BookingId+'"}'; 
    console.log(query);
    method = "get";
    if (method.length == 0) { 
        console.log("wrong method")
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                
                    console.log(this.responseText);

                 var obj = JSON.parse(this.responseText);

                if(obj.status.match("success"))
                {
                    paymentSummaryGenerator(obj['data'][0]);
                }else{
                    alert(obj.error);
                }
            }
        };
        xmlhttp.open("GET", "EndPoint.php?query="+query, true);
        xmlhttp.send();
    }
}