// function to get the variables from the url
function GetUrlValue(VarSearch){
    var SearchString = window.location.search.substring(1);
    var VariableArray = SearchString.split('&');
    for(var i = 0; i < VariableArray.length; i++){
        var KeyValuePair = VariableArray[i].split('=');
        if(KeyValuePair[0] == VarSearch){
            return KeyValuePair[1];
        }
    }
}


//var custom = '{"ID": 1 , "Title" : "Looi", "Description": "bla","Price": 100, "Itinerary":[{"time": "1000","event":"hey"},{"time": "1200","event":"bla"}], "TermsCondition": "bla"}';
// get the value for package id from the previous page
var package_id = GetUrlValue('packageId');


Query(package_id);

function customGenerator(custom){
	var custom_obj = custom;

    document.getElementById("custom_image").src = custom_obj.Image;
    document.getElementById("custom_image").style.height = "300px";
    document.getElementById("custom_image").style.width = "500px";
    document.getElementById("custom_image").style.marginLeft = "auto";
	document.getElementById("custom_image").style.marginRight = "auto";
    // console.log(custom_obj.Itinerary[0]);

	var custom_image;
	var package_details_title = document.getElementById("package_details_title");
	package_details_title.innerHTML = custom_obj.Title;

	var package_details_des = document.getElementById("package_details_des");
	package_details_des.innerHTML = custom_obj.Description;

	var package_details_price = document.getElementById("package_details_price");
	package_details_price.innerHTML =  "RM " + custom_obj.Price;

	var package_details_it = document.getElementById("package_details_it");


	for(var key in custom_obj.Itinerary)
	{
	    var table_row = document.createElement("tr");
	    var time = document.createElement("td");
	    var events = document.createElement("td");

	    time.innerHTML = custom_obj.Itinerary[key].time;
	    events.innerHTML = custom_obj.Itinerary[key].event;
	    table_row.appendChild(time);
	    table_row.appendChild(events);
	    package_details_it.appendChild(table_row);
	}

	var package_details_tnc = document.getElementById("package_details_tnc");
	package_details_tnc.innerHTML = custom_obj.TermsCondition;

}

function orderPackageDetails()
{
    // onclick to call the custom html and id need to be passed to this page
    window.location = "make.html?packageId="+package_id;
}


function Query(package_id){
    var query='{ "table": "package", "filter": "Id = '+package_id+'" }'; 
    console.log(query);
    method = "get";
    if (method.length == 0) { 
        console.log("wrong method")
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                
                    console.log(this.responseText.replace(/\\\//g, ""));

                 var obj = JSON.parse(this.responseText);
                 
                if(obj.status.match("success"))
                {	
                	obj.data[0].Itinerary = JSON.parse(obj.data[0].Itinerary.replace(/\\\//g, ""));
                    customGenerator(obj['data'][0]);
                }else{
                    
                    document.getElementById("custom_title").innerHTML = "Error 404 : Not Found";
                    document.getElementById("custom_container").innerHTML = "NO RESULT";
                	document.getElementById("custom_container").style.height = "80%";
                	alert(obj.error);
                }
            }
        };
        xmlhttp.open("GET", "EndPoint.php?query="+query, true);
        xmlhttp.send();
    }
}