
Query() ;
function packageGenerator(myObj){

    for(var i = 0; i < myObj.length; i++)
    {
        var j = myObj[i];

        
         var packages_container = document.createElement("div");
        packages_container.setAttribute("id","packages_container");
        packages_container.setAttribute("name",j.Id);
        packages_container.setAttribute("onclick","orderPackage(this.getAttribute('name'), this.id)");


        var image_container = document.createElement("div");
        image_container.setAttribute("id", "image_container");
        image_container.classList.add("image_container");

        var image = document.createElement("img");
        image.style.height = "100%";
        image.style.width = "100%";
        image.style.maxHeight = "200px";
        image.setAttribute("id", "menu_image");
        image.src = j.Image;
        image.setAttribute("alt", "package_image");


        

        var packages_details = document.createElement("div");
        packages_details.setAttribute("id","packages_details");
        packages_details.classList.add("packages_details");



        var package_name_container = document.createElement("div");
        package_name_container.setAttribute("id", "package_name_container");
        package_name_container.classList.add("package_name_container");
        var package_name = document.createElement("p");
        package_name.innerHTML = "Name: " + j.Title;
        package_name_container.appendChild(package_name);

        var des_container = document.createElement("div");
        des_container.setAttribute("id", "des_container");
        des_container.classList.add("des_container");
        var des = document.createElement("p");
        des.innerHTML = "Description: " + j.Description;
        des_container.appendChild(des);

        var price_container = document.createElement("div");
        price_container.setAttribute("id", "price_container");
        price_container.classList.add("price_container");
        var price = document.createElement("p");
        price.innerHTML = "Price: RM" + j.Price;
        price_container.appendChild(price);

        packages_details.appendChild(package_name_container);
        packages_details.appendChild(des_container);
        packages_details.appendChild(price_container);

        image_container.appendChild(image);
        packages_container.appendChild(image_container);
        packages_container.appendChild(packages_details);


        document.getElementById("packages_outer").appendChild(packages_container);


    }

}


// function to get the package id to pass to custom page
// package_id is the parameter that get the id of the package 
// container_id is used for styling purpose

function orderPackage(package_id, container_id)
{
    // onclick to call the custom html and id need to be passed to this page
    window.location = "custom.html?packageId="+package_id;
}

function Query(){
    var query='{ "table": "package"}' 
    console.log(query);
    method = "get";
    if (method.length == 0) { 
        console.log("wrong method")
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                
                    console.log(this.responseText);

                 var obj = JSON.parse(this.responseText);

                if(obj.status.match("success"))
                {
                    packageGenerator(obj['data']);
                }else{
                    alert(obj.error);
                }
            }
        };
        xmlhttp.open("GET", "EndPoint.php?query="+query, true);
        xmlhttp.send();
    }
}