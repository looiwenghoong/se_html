var packages = '{"Tour": "Pankgor", "Description": "bla", "Price":100, "TotalPrice":"123"}';
var payment = '{"Name":"Looi Weng Hoong", "CardType": "Visa", "Address": "bla", "ContactNo": "0123456789"}';
var additional = '{"BookingId": "BK1234","HotelName": "a", "Description": "bla", "LunchTitle": "bla", "DinnerTitle": "bla", "NoOfPax": 3, "BookingDate":"28-04-2018"}';


// var packagesObj = JSON.parse(packages);
// var additionalObj = JSON.parse(additional);
// var paymentObj = JSON.parse(payment);

Query(GetUrlValue("bookingId"));

function invoiceGenerator(Obj){
	
	// package details
	var booking_id = document.getElementById("booking_id");
	booking_id.innerHTML = "Booking ID: " + Obj.BookingId;

	var booking_date = document.getElementById("booking_date");
	booking_date.innerHTML = "Placed On: " + Obj.BookingDate;


	var package_title = document.getElementById("package_title");
	package_title.innerHTML = Obj.Tour;

	var package_description = document.getElementById("package_description");
	package_description.innerHTML = Obj.Description;

	var package_price = document.getElementById("package_price");
	package_price.innerHTML = "RM "+ Obj.Price + " Per Person";

	var package_person = document.getElementById("package_person");
	package_person.innerHTML = Obj.NoOfPax;

	var package_total = document.getElementById("total");
	package_total.innerHTML = "RM "+Obj.TotalPrice;


	// additional details
	var hotel_name = document.getElementById("hotel_name");
	hotel_name.innerHTML = Obj.HotelName;

	var hotel_details = document.getElementById("hotel_details");
	hotel_details.innerHTML = Obj.Description;

	var lunch= document.getElementById("lunch");
	lunch.innerHTML = Obj.LunchTitle;

	var dinner = document.getElementById("dinner");
	dinner.innerHTML = Obj.DinnerTitle;



	// payment details
	var pay_name = document.getElementById("payment_details_name");
	pay_name.innerHTML = Obj.Name;

	var pay_method = document.getElementById("payment_details_method");
	pay_method.innerHTML = "Paid by "+ Obj.CardType;

	var pay_add = document.getElementById("payment_details_address");
	pay_add.innerHTML = Obj.Address;

	var pay_contact = document.getElementById("payment_details_contact");
	pay_contact.innerHTML = Obj.ContactNo;

}

function GetUrlValue(VarSearch){
    var SearchString = window.location.search.substring(1);
    var VariableArray = SearchString.split('&');
    for(var i = 0; i < VariableArray.length; i++){
        var KeyValuePair = VariableArray[i].split('=');
        if(KeyValuePair[0] == VarSearch){
            return KeyValuePair[1];
        }
    }
}

function Query(BookingId){
    var query='{ "table": "custom","custom":"SELECT booking.Id as BookingId,package.Title as Tour,package.Description ,package.Price,payment.CardHolderName as Name,payment.CardType,payment.Address,payment.PhoneNumber as ContactNo,payment.Price as TotalPrice,booking.Id as BookindId,hotel.HotelName,hotel.Description,lunch.Title as LunchTitle ,dinner.Title AS DinnerTitle,booking.NoOfPax,booking.BookingDate FROM `booking` as booking INNER JOIN package as package on booking.PackageId = package.Id INNER JOIN hotel as hotel on booking.HotelId = hotel.Id INNER JOIN food as lunch ON LunchId = lunch.Id INNER JOIN food as dinner ON DinnerId = dinner.Id INNER JOIN payment as payment ON Payment = payment.Id  WHERE booking.Id = '+BookingId+'"}'; 
    console.log(query);
    method = "get";
    if (method.length == 0) { 
        console.log("wrong method")
        return;
    } else {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                
                    console.log(this.responseText);

                 var obj = JSON.parse(this.responseText);

                if(obj.status.match("success"))
                {
                	
                    invoiceGenerator(obj['data'][0]);
                }else{
                    alert(obj.error);
                }
            }
        };
        xmlhttp.open("GET", "EndPoint.php?query="+query, true);
        xmlhttp.send();
    }
}
