var sIndex = 0;
Slider();

function Slider() {
    var i;
    var slides = document.getElementsByClassName("slideshow_content");
    var dots = document.getElementsByClassName("dot");
	for (i = 0; i < slides.length; i++) 
	{
	   slides[i].style.display = "none";  
	}

	for(i = 0; i< dots.length; i++)
	{
		dots[i].className = dots[i].className.replace("active", "");	
	}

	slides[sIndex].style.display = "block";
	dots[sIndex].className += " active";

	sIndex++;
	if(sIndex == slides.length)
	{
		sIndex = 0;
	}

	// Recursive function to call the function repeatly
	setTimeout(Slider, 3500);
}
