
// Get email input
var Email = document.getElementById("form_email");

// check if the input is in the valid email format
Email.onchange = function (e) 
{
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if (document.getElementById("form_email").value.match(mailformat))
	{
		document.getElementById("form_email_error").style.display = "none";
	}
	else
	{
		document.getElementById("form_email_error").style.display = "block";
	}        
};


// get retype password input
var retype = document.getElementById("form_retype");

// check if the retype password input is matched with the previous
// password
retype.onchange = function (e)
{
	var pass = document.getElementById("form_password").value;

	if(retype.value.match(pass))
	{
		document.getElementById("form_retype_error").style.display = "none";
	}
	else
	{
		document.getElementById("form_retype_error").style.display = "block";
	}
}

// reset the form when the page is refreshed
if (performance.navigation.type == 1) 
{
    document.getElementById("account_form").reset();
}

// // show payment history button trigger
// function showPHistory()
// {
//     var account_pHistory_content = document.getElementById("scrolling");
//     var pHistory_button = document.getElementById("account_pHistory");

//     if(account_pHistory_content.style.display == "")
//     {
//         account_pHistory_content.style.display = "block";
//         account_pHistory.innerHTML = "Close Payment History";
//     }
//     else
//     {
//         account_pHistory_content.style.display = "";
//         account_pHistory.innerHTML = "View Payment History";
//     }
// }

// function to hide display and show edit
function editAcc()
{
    var a = document.getElementById("account_container");
    var b = document.getElementById("account_container_edit");

    a.style.display = "none";
    b.style.display = "block";
}

// funcition to show display and hide edit
function backtoAcc()
{
    var a = document.getElementById("account_container");
    var b = document.getElementById("account_container_edit");

    a.style.display = "block";
    b.style.display = "none"
}

function checkCookie()
{   
	var account = getCookie("accountCookie");

	// console.log(JSON.parse(account));
	if(account != "" || account != null)
	{
		var user=JSON.parse(account);
        document.getElementById("navbar_account").innerHTML = user.data.Username;
        
        var img = document.getElementById("profile_pic");
        img.src = JSON.parse(getCookie("accountCookie")).data.Image;
        
        document.getElementById("form_first").value = JSON.parse(getCookie("accountCookie")).data.FirstName;
        document.getElementById("form_second").value = JSON.parse(getCookie("accountCookie")).data.LastName;
        document.getElementById("form_email").value = JSON.parse(getCookie("accountCookie")).data.Email;
        document.getElementById("form_uName").innerHTML = JSON.parse(getCookie("accountCookie")).data.Username;

        document.getElementById("account_fName").innerHTML = JSON.parse(getCookie("accountCookie")).data.FirstName;
        document.getElementById("account_lName").innerHTML = JSON.parse(getCookie("accountCookie")).data.LastName;
        document.getElementById("account_uName").innerHTML = JSON.parse(getCookie("accountCookie")).data.Username;
        document.getElementById("account_email").innerHTML = JSON.parse(getCookie("accountCookie")).data.Email;
	}
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);

    console.log(decodedCookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return null;
}

function setCookie(cname, cvalue, cexdays)
{
    var d = new Date();
    d.setTime(d.getTime() + (cexdays*24*60*60*1000));
    var expires = "expires=" + d.toGMTString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function eraseCookie() {
    document.cookie = 'accountCookie=; Max-Age=0'
    console.log("erased");
}

function logout()
{
    // erase the cookie
    eraseCookie();
    window.open("homePage.html", "_self");
}


var dummy = '{"status": "success", "data": {"Id": 1, "FirstName" : "looi", "LastName" : "Weng Hoong", "Username" : "Looi", "Email": "looiwenhoon@yahoo.com"}}';
// Update the account details
function updateProfile()
{   
    var accountDetail = document.getElementById('account_form');
    var formData = new FormData(accountDetail);
    if(!formData.get("Password")||!formData.get("retype")){
            formData.delete("Password")
    }else{
        if(formData.get("Password")==formData.get("retype")){
                alert("Password is Not Match");
                return;
        }
    }
            formData.append("Id",JSON.parse(getCookie("accountCookie")).data.Id);
            formData.delete("retype");
            QueryPost(formData,accountDetail.action);    
    
    //
    // var form_data =new FormData(document.getElementById("account_form"));
    
}

function gotoBook()
{
    window.location = "myBooking.html";
}

function QueryPost(formData,action){
    var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                console.log(this.responseText);
                var data = JSON.parse(this.responseText);
                data.data = data.data[0];
                console.log(JSON.stringify(data));
                eraseCookie();
                setCookie("accountCookie", JSON.stringify(data), 30);
                location.reload();
            }
        };
    xmlhttp.open("post", action);
    
    formData.append("table","clientuser");
    
    xmlhttp.send(formData);
}